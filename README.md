# Простой чат на Django channels

0) Для работы приложения, в системе должен быть корректно установлен [Redis](http://redis.io/) и работать по адресу localhost:6379 без авторизации.

Устанавливается командой:
```sh
sudo apt-get install redis-server
```

1) создаем виртуальное окружение и устанавливаем зависимости из файла `requirements.txt`
```sh
virtualenv --python=python3 .env
source .env/bin/activate
pip install -r requirements.txt
```

3) Выполняем миграцию.
```sh
cd testjob/
./manage.py migrate
```

4) Запускаем Django
```sh
./manage.py runserver
```
