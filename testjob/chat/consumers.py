import json

from channels import Group, Channel
from channels.auth import channel_session_user_from_http, channel_session_user
import redis
import logging

logger = logging.getLogger(__name__)


@channel_session_user_from_http
def ws_add(message):
    r = redis.Redis()
    all_messages = r.lrange('chat:messages:list', 0, -1)
    if all_messages:
        try:
            decoded_all_messages = [msg.decode('utf8') for msg in all_messages]
            message.reply_channel.send({"text": json.dumps(decoded_all_messages)})
        except Exception:
            logger.error('Messages from Redis was not decoded and sent!')
    Group("chat").add(message.reply_channel)
    msg = "User {user} is in the chat.".format(user=message.user.username)
    Group("chat").send({"text": '["{}"]'.format(msg)})


@channel_session_user
def ws_message(message):
    # r = redis.Redis()
    msg = "{user} --> {text}".format(user=message.user.username, text=message.content['text'])
    Group("chat").send({
        "text": '["{}"]'.format(msg),
    })
    Channel("save_msg").send({'msg': msg})


@channel_session_user
def ws_disconnect(message):
    msg = "User {user} is out.".format(user=message.user.username)
    Group("chat").send({"text": '["{}"]'.format(msg)})
    Group("chat").discard(message.reply_channel)


def save_msg(message):
    msg = message.content['msg']
    r = redis.Redis()
    result = r.rpush('chat:messages:list', msg)
    if result:
        logger.info('Message {} has been successfully saved to Redis'.format(msg))
    else:
        logger.error('Message {} has not been saved to Redis'.format(msg))
