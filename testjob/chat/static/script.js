$(document).ready(function() {

    socket = new WebSocket("ws://" + window.location.host + "/chat/");

    socket.onmessage = function(e) {
        var received_msg = JSON.parse(e.data);
        received_msg.forEach(function(item) {
            $('form').after(`<div class="well well-sm">${item}</div>`);
        });
    }

    $('button').on('click', function() {
        var field = $('textarea');
        var msg = field.val();
        socket.send(msg);
        field.val('');
    })

});
